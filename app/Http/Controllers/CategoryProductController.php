<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoriesStoreRequest;
use App\Http\Requests\CategoriesRequest;
use App\Http\Resources\CategoriesResource;
use App\Models\Categories;
use Illuminate\Http\Request;

class CategoryProductController extends Controller
{
    /**
     * Displays a lists of resources
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return CategoriesResource::collection(Categories::all());
    }

    /**
     * Store a new created resource in the storage
     * @param CategoriesRequest $request
     * @return CategoriesResource
     */
    public function store(CategoriesRequest $request)
    {
        return new CategoriesResource(Categories::create($request->validated()));
    }

    /**
     * Display the specified resource (by its id)
     * @param $id
     * @return CategoriesResource
     */
    public function show($id)
    {
        return new CategoriesResource(Categories::findOrFail($id));
    }

    /**
     * Update/Edit the specified resource (by its id) in storage
     * @param CategoriesRequest $request
     * @param $id
     * @return CategoriesResource
     */

    public function update(CategoriesRequest $request, $id)
    {
        $categories = Categories::findOrFail($id);
        $attributes = $request->validated();
        $categories->update($attributes);
        return new CategoriesResource($categories);
    }

    /**
     * Remove the specified resource (by id) from storage
     * @param $id
     * @return CategoriesResource
     */
    public function delete($id)
    {
        $categories = Categories::findOrFail($id);
        $categories->delete();
        return new CategoriesResource($categories);
    }
}
