<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * handle user registration request
     * @param RegisterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function registerUser(RegisterRequest $request)
    {
        $attributes = $request->validated();
        $user = User::create($attributes);
        $access_token = $user->createToken('PassportToken')->accessToken;
        return response()->json(['token' => $access_token], 200);
    }

    /**
     * login user to application
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginUser(LoginRequest $request)
    {
        $attributes = $request->validated();

        if (auth()->attempt($attributes)) {

            $user_login_token = auth()->user()->createToken('PassportToken')->accessToken;

            return response()->json(['token' => $user_login_token], 200);
        }
        else {
            return response()->json(['error' => 'UnAuthorised Access'], 401);
        }
    }
}
