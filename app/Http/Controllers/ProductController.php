<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    /**
     * Displays a lists of resources
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return ProductResource::collection(Product::all());
    }

    /**
     * Store a new created resource in the storage
     * @param ProductRequest $request
     * @return ProductResource
     */
    public function store(ProductRequest $request)
    {
        return new ProductResource(Product::create($request->validated()));
    }

    /**
     * Display the specified resource (by its id)
     * @param $id
     * @return ProductResource
     */
    public function show($id)
    {
        return new ProductResource(Product::findOrFail($id));
    }

    /**
     * Update/Edit the specified resource (by its id) in storage
     * @param ProductRequest $request
     * @param $id
     * @return ProductResource
     */

    public function update(ProductRequest $request, $id)
    {
        $product = Product::findOrFail($id);
        $attributes = $request->validated();
        $product->update($attributes);
        return new ProductResource($product);
    }

    /**
     * Remove the specified resource (by id) from storage
     * @param $id
     * @return ProductResource
     */
    public function delete($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return new ProductResource($product);
    }
}
