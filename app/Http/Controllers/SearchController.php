<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
     * Search function that returns trips by specific criteria + paginates
     * @param $products
     * @return array
     */
    public function searchFilter(Request $request)
    {

        $products = new Product();

        //searching based on product/description
        if ($s = $request->input('s')) {
            $products = $products->where(function ($search) use ($s) {
                $search->where('product', 'like', '%' . $s . '%')
                    ->orWhere('description', 'like', '%' . $s . '%')
                    ->get();
            });
        }

        //searching based on colour
        if ($colour = $request->input('colour')) {
            $products = $products->where(function ($c) use ($colour) {
                $c->where('colour', 'like', '%' . $colour . '%')
                    ->get();
            });
        }

        //filtering based on minimum price
        if ($min = $request->input('min')) {
            $products = $products->where(function ($minq) use ($min) {
                $minq->where('price', '>=', $min)->get();
            });
        }

        //filtering based of maximum price
        if ($max = $request->input('max')) {
            $products = $products->where(function ($maxq) use ($max) {
                $maxq->where('price', '<=', $max)->get();
            });
        }

        //filtering based on in_stock
        if($stock = $request->input('in_stock')){
            $products = $products->where(function ($in) use($stock){
               $in->where('in_stock', '=', $stock)->get();
            });
        }

        //filtering based on out_of_stock
        if($stock = $request->input('out_of_stock')){
            $products = $products->where(function ($out) use($stock){
                $out->where('out_of_stock', '=', $stock)->get();
            });
        }

        //sorting adc/des based on price
        if ($sort = $request->input('sortprice')) {
            $products = $products->orderBy('price', $sort );
        }

        //sorting adc/des based on product
        if ($sort = $request->input('sortproduct')) {
            $products = $products->orderBy('product', $sort);
        }


        //Pagination
        $pPage = 5;
        $page = $request->input('page', 1);
        $total = $products->count();
        $result = $products->offset(($page - 1) * $pPage)->limit($pPage)->get();

        return [
            'data' => $result,
            'total' => $total,
            'page' => $page,
            'last_page' => ceil($total / $pPage)
        ];
    }
}
