<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'product' => 'required|unique:products,product',
                'description' => 'required|min:10|max:255',
                'price' => 'required',
                'number' => 'required',
                'in_stock' => 'required',
                'out_of_stock' => 'required'
        ];
    }


}
