<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|min:3|max:255',
            'password' => 'required|max:255'
        ];
    }

    /**
     * Personalized messages for request
     * @return string[]
     */
    public function messages()
    {
        return [
            'email.required' => 'Introduceti emailul',
            'email.email' => 'Trebuie sa fie de tip email',
            'email.max' => 'Emailul trebuie sa aiba cel mult 255 de caractere',
            'email.min' => 'Emailul trebuie sa aiba cel putin 3 caractere',
            'password.required' => 'Introduceti parola',
            'password.max' => 'Parola trebuie sa contina cel mult 255 de caractere'
        ];
    }
}
