<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:categories,name|min:3|max:255'
        ];
    }

    /**
     * Personalized messages for requests
     * @return string[]
     */
    public function messages()
    {
        return [
            'name.required' => 'Introduceti numele categoriei',
            'name.unique' => 'Aceasta categorie se afla deja in baza de date',
            'name.min' => 'Categoria trebuie sa aiba cel putin 3 caractere',
            'name.max' => 'Categoria trebuie sa aiba cel mult 255 de caractere',
        ];
    }
}
