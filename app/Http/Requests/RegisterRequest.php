<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|min:3|max:255',
            'last_name' => 'required|min:3|max:255',
            'nickname' => 'required|min:3|max:255|unique:users,nickname',
            'email' => 'required|email|min:3|max:255|unique:users,email',
            'password' => 'required|min:7|max:255'
        ];
    }

    /**
     * Personalized messages for requests
     * @return string[]
     */
    public function messages()
    {
        return [
            'first_name.required' => 'Introduceti Prenumele',
            'first_name.min' => 'Prenumele trebuie sa aiba cel putin 3 caractere',
            'first_name.max' => 'Prenumele trebuie sa aiba cel mult 255 de caractere',
            'last_name.required' => 'Introduceti Numele',
            'last_name.min' => 'Numele trebuie sa aiba cel putin 3 caractere',
            'last_name.max' => 'Numele trebuie sa aiba cel mult 255 de caractere',
            'nickname.required' => 'Introduceti Nickname-ul',
            'nickname.min' => 'Nickname-ul trebuie sa aiba cel putin 3 caractere',
            'nickname.max' => 'Nickname-ul trebuie sa aiba cel mult 255 de caractere',
            'nickname.unique' => 'Acest nickname exista deja in baza de date',
            'email.required' => 'Introduceti emailul',
            'email.max' => 'Emailul trebuie sa aiba cel mult 255 de caractere',
            'email.min' => 'Emailul trebuie sa aiba cel putin 3 caractere',
            'email.email' => 'Trebuie sa fie de tip email',
            'email.unique' => 'Acest email exista deja in baza de date',
            'password.required' => 'Introduceti parola',
            'password.min' => 'Parola trebuie sa aiba cel putin 3 caractere',
            'password.max' => 'Parola trebuie sa aiba cel mult 255 de caractere',
        ];
    }
}
