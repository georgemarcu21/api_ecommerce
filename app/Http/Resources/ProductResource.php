<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'product' => $this->product,
            'description' => $this->description,
            'price' => $this->price,
            'number' => $this->number,
            'in_stock' => $this->in_stock,
            'out_of_stock' => $this->out_of_stock,
            'created_at' => $this->created_at,
            'updated-at' => $this->updated_at,
        ];
    }
}
