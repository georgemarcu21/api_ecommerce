<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Guest section

//Hello route
Route::get('/hello', function () {
    $welcome = 'welcome here';
    return response()->json($welcome);
});

//Register/Login routes
Route::post('/register', 'AuthController@registerUser');
Route::post('/login', 'AuthController@loginUser');

Route::get('/email/resend', 'VerificationController@resend')->name('verification.resend');
Route::get('/email/verify/{id}/{hash}', 'VerificationController@verify')->name('verification.verify');

// Authentificated
Route::middleware('auth:api')->group(function () {

    // Admin section
    Route::middleware(['auth', 'role:admin'])->group(function () {

        //Products CRUD
        Route::get('/products', 'ProductController@index');
        Route::post('/product', 'ProductController@store');
        Route::get('/products/{id}', 'ProductController@show');
        Route::put('/products/{id}', 'ProductController@update');
        Route::delete('/products/{id}', 'ProductController@delete');

        //Categories CRUD
        Route::get('/categories', 'CategoryProductController@index');
        Route::post('/category', 'CategoryProductController@store');
        Route::get('/categories/{id}', 'CategoryProductController@show');
        Route::put('/categories/{id}', 'CategoryProductController@update');
        Route::delete('/categories/{id}', 'CategoryProductController@delete');

        //Search route (for products/categories)
        Route::get('/search', 'SearchController@searchFilter');
    });


    // Client section
    Route::middleware(['auth', 'role:client'])->group(function () {

    });
});
