<?php

namespace Database\Factories;

use App\Models\Categories;
use App\Models\product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'product' => $this->faker->text(),
            'description' => $this->faker->text(),
            'category_id' => Categories::factory(),
            'price' => $this->faker->randomNumber(3),
            'number'=>$this->faker->randomNumber(2),
            'in_stock' => $this->faker->boolean(),
            'out_of_stock' => $this->faker->boolean(),
            'colour' => $this->faker->colorName(),
        ];
    }
}
